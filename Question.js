// creation des objets questions

class QQuestion {
//declaration de quesceque question peut conporter
    constructor(texte, choix, reponse) {
        this._texte = texte;
        this._choix = choix;
        this._reponse = reponse;
    }


    get texte() {
        return this._texte;
    }

    set texte(value) {
        this._texte = value;
    }

    get choix() {
        return this._choix;
    }

    set choix(value) {
        this._choix = value;
    }

    get reponse() {
        return this._reponse;
    }

    set reponse(value) {
        this._reponse = value;
    }
    BonneReponse(choi) {
        return this.reponse === choi;
    };
}