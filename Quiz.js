class Quizz {
constructor(questions) {
        this._score = 0;
        this._questionIndex = 0;
        this._questions = questions;
    }
    get questions() {
        return this._questions;
    }

    set questions(value) {
        this._questions = value;
    }

    get score() {
        return this._score;
    }

    set score(value) {
        this._score = value;
    }

    get questionIndex() {
        return this._questionIndex;
    }

    set questionIndex(value) {
        this._questionIndex = value;
    }


      guess=function (reponse) {
        if (this.getQuestionIndex().BonneReponse(reponse)) {
            this.score++;
        }

        this.questionIndex++;
    };


    EstFini = function () {
        return this.questionIndex === this.questions.length;
    };


}