
function Rejouer_Partie(){
    window.location.reload();
}
let rejoue = document.getElementById("Rejouer")
rejoue.addEventListener("click",Rejouer_Partie);










//contructeur de Quiz
"use strict";

function Quiz(questions) {
    this.score = 0;
    this.questions = questions;
    this.questionIndex = 0;
}



// prototype sert a faire de lheritage 
//foncition qui retourne lindex de la question

Quiz.prototype.getQuestionIndex = function () {
    return this.questions[this.questionIndex];

};

// sert a voir si la reponse est bonne et si oui ajout de point sinon point reste tel que tel et fait monter lindex de la progression
Quiz.prototype.guess = function (reponse) {
    if (this.getQuestionIndex().BonneReponse(reponse)) {
        this.score++;
    }

    this.questionIndex++;
};

// qui verifie si on arrive à la fin des questions
Quiz.prototype.EstFini = function () {
    return this.questionIndex === this.questions.length;
};

//declaration de quesceque question peut conporter
/*function Question(texte, choix, reponse) {
    this.texte = texte;
    this.choix = choix;
    this.reponse = reponse;
}*/

//methode qui verifie si la reponse est bonne
QQuestion.BonneReponse = function (choi) {
    return this.reponse === choi;
};

//fonction qui montrte les question et les choix
function Affichage() {
    if (quiz.EstFini()) {
        AfficheResultat();
    } else {
        // affiche les questions
        var element = document.getElementById("question");
        element.innerHTML = quiz.getQuestionIndex().texte;

        // affiche options
        var choix = quiz.getQuestionIndex().choix;
        for (var i = 0; i < choix.length; i++) {
            var element = document.getElementById("choice" + i);
            element.innerHTML = choix[i];
            ChoisiElement("btn" + i, choix[i]);
        }

        AfficheLaProgression();
    }
}


//affiche où nous sommes rendu dans la progression du quiz
function AfficheLaProgression() {
    var ProgessionQuestion = quiz.questionIndex + 1;
    var element = document.getElementById("progression");
    element.innerHTML = "Question " + ProgessionQuestion + "/" + quiz.questions.length;
}


// fonction qui affiche le resultats 

function AfficheResultat() {
    var PartieFini = "<h1>Resultats</h1>";
    PartieFini += "<h2 id='score'> Votre resultat: " + quiz.score + "</h2>  ";
    PartieFini += "<button id='Rejouer'> Rejouer " + "</button>";
    var element = document.getElementById("quiz");
    element.innerHTML = PartieFini;
    let rejoue = document.getElementById("Rejouer")
    rejoue.addEventListener("click",Rejouer_Partie);


}


// fonction qui prend un id et prend notre reponse et passe a la prochaine question si il en a une de prensente

function ChoisiElement(id, Elemnt) {
    var button = document.getElementById(id);
    button.onclick = function () {
        quiz.guess(Elemnt);
        Affichage();
    }
}

// creation des objets questions
let questions = [
    new QQuestion("Qui a invente le systeme d'exploitation Linux ?", ["Linus Torvalds", "Steve Job", "Bill Gates", "Jeff Bezos"], "Linus Torvalds"),
    new QQuestion("Quel application ne fait pas partie de la suite office ?", ["Word", "Excel", "Photoshop", "Powerpoint"], "Photoshop"),
    new QQuestion("Quel Information est fausse ? ", ["Microsoft possede GitHub ", "Tous les outils IDE Java utilisent GIT", "Apple n'utilise pas Git avec Xcode", "En 2018 Git comptait environ 600 000 depots"], "Apple n'utilise pas Git avec Xcode"),
    new QQuestion("Lequel de ces languages de programmation est un language Serveur ? ", ["PHP", "HTML", "Javascript", "SqlServer"], "PHP"),
    new QQuestion("Quel syteme d'exploitation ne fait pas partie de la famille de GNU ? ", ["Ubuntu", "Debian", "Kali Linux", "Windows"], "Windows")

];

// cree un objet quiz
var quiz = new Quiz(questions);


//Affichage du quiz
Affichage();
